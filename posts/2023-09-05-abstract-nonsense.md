---
title: Abstract nonsense
keywords: ["art", "theory"]
---

I've been reading The Joy of Abstraction, by Eugenia Cheng. Very accessible. Would recommend.
It's doing good stuff to my mind.

---

= Abstraction, food for thought

Two apples are the same as two apples.

Two apples are not the same as two oranges.

Two ripe apples are not the same as two rotten apples, even though they are both two apples and two apples.

Two fruits are the same as two fruits, even though they could be two apples and two oranges.
