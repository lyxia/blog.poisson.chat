// type Tape = Immutable.Map<number, number>
// type State = { tape: Tape, head: number }
// // Regexes are interpreted as nondeterministic computations
// type Expr = State -> Immutable.Seq<State>

// Empty string = identity function (no change to state)
const epsilonExpr = (state) => Immutable.Seq([state])
function Sequence(exprList) {
    if (exprList.length === 0) {
        return epsilonExpr
    } else if (exprList.length === 1) {
        return exprList[0]
    } else {
        return (state) => exprList.reduce(
            (states, expr) => states.flatMap(m => expr(m)),
            Immutable.Seq([state]))
    }
}
// Union of regexes = union of nondeterministic states
const Alt = (expr1, expr2) => state =>
    Immutable.Seq([expr1, expr2]).flatMap(expr => expr(state))
// a* = (ε|aa*)
function Star(expr) {
    let f;
    f = Alt(epsilonExpr,Sequence([expr,(state) => f(state)]));
    return f
}
// a+ = aa*
function Plus(expr) {
    return Sequence([expr, Star(expr)])
}
const AssertRead = (n) => state => {
    const readValue = state.tape.get(state.head, 0);
    if (n === readValue) {
        return Immutable.Seq([state])
    } else {
        return Immutable.Seq([])
    }
}
const AssertReadNeq = (n) => state => {
    const readValue = state.tape.get(state.head, 0);
    if (n !== readValue) {
        return Immutable.Seq([state])
    } else {
        return Immutable.Seq([])
    }
}
const Write = (n) => state => Immutable.Seq([{
    head: state.head,
    tape: state.tape.set(state.head, n),
}])
const MoveLeft = (state) => {
    if (state.head === -100) {
        throw state
    }
    return Immutable.Seq([{
        head: state.head - 1,
        tape: state.tape,
    }])
}
const MoveRight = (state) => {
    if (state.head === 100) {
        throw state
    }
    return Immutable.Seq([{
        head: state.head + 1,
        tape: state.tape,
    }])
}
const Incr = (state) => Immutable.Seq([{
    head: state.head,
    tape: state.tape.update(state.head, 0, n => (n + 1) % 256)
}])
const Decr = (state) => Immutable.Seq([{
    head: state.head,
    tape: state.tape.update(state.head, 0, n => (n - 1) % 256)
}])
// brainfuck "[...]", desugars to "(0/...)*0?"
const WhileZ = (expr) =>
    Sequence([Star(Sequence([AssertReadNeq(0), expr])), AssertRead(0)])

function parseExpr(src) {
    src = src.replace(/(#[^\n]*(\n|$))|\s*/g, '');
    // stack: A linked list of partial expressions.
    // stack['prev'] is the back pointer.
    // if stack['symbol'] === '(', we are parsing a subexpression delimited by an open parenthesis.
    // if stack['symbol'] === '|', we are parsing the right operand of '|'.
    let stack = null;
    let currentSequence = [];
    for (var i = 0; i < src.length; i++) {
        if (src[i] === '(') {
            stack = { symbol: '(', position: i, sequence: currentSequence, prev: stack };
            currentSequence = [];
        } else if (src[i] === '[') {
            stack = { symbol: '[', position: i, sequence: currentSequence, prev: stack };
            currentSequence = [];
        } else if (src[i] === ')') {
            let expr = Sequence(currentSequence);
            while (true) {  // break when we find the matching '('
                if (stack === null) {
                    return `Syntax error: mismatched ')' at position ${i}`
                }
                const sc = stack.symbol;
                if (sc === '(') {
                    currentSequence = stack.sequence;
                    if (expr !== epsilonExpr) {
                        currentSequence.push(expr);
                    }
                    stack = stack.prev;
                    break;
                } else if (sc === '|') {
                    expr = Alt(stack.left, expr);
                    stack = stack.prev;
                } else if (sc === '[') {
                    return `Syntax error: mismatched ')' at position ${i} (open '[' at ${stack.position})`
                } else {
                    return `The impossible happened at position ${i}`
                }
            }
        } else if (src[i] === ']') {
            let expr = Sequence(currentSequence);
            while (true) {  // break when we find the matching '['
                if (stack === null) {
                    return `Syntax error: mismatched ']' at position ${i}`
                }
                const sc = stack.symbol;
                if (sc === '[') {
                    currentSequence = stack.sequence;
                    if (expr !== epsilonExpr) {
                        currentSequence.push(WhileZ(expr));
                    }
                    stack = stack.prev;
                    break;
                } else if (sc === '|') {
                    expr = Alt(stack.left, expr);
                    stack = stack.prev;
                } else if (sc === '(') {
                    return `Syntax error: mismatched ']' at position ${i} (open '(' at ${stack.position})`
                } else {
                    return `The impossible happened at position ${i}`
                }
            }
        } else if (src[i] === '|') {
            stack = { 'symbol': '|', 'position': i, 'left': Sequence(currentSequence), 'prev': stack };
            currentSequence = [];
        } else if (src[i] === '*') {
            const len = currentSequence.length;
            if (len === 0) {
                return `Syntax error: no operand for '*' at position ${i}`
            }
            currentSequence[len-1] = Star(currentSequence[len-1]);
        } else if (src[i] === '\\') {
            i++;
            if (!(i < src.length && src[i] === '+')) {
                return `Syntax error: missing '+' after backslash at position ${i}`
            }
            const len = currentSequence.length;
            if (len === 0) {
                return `Syntax error: no operand for '+' at position ${i}`
            }
            currentSequence[len-1] = Plus(currentSequence[len-1]);
        } else if (src[i] === '<') {
            currentSequence.push(MoveLeft);
        } else if (src[i] === '>') {
            currentSequence.push(MoveRight);
        } else if (src[i] === '+') {
            currentSequence.push(Incr);
        } else if (src[i] === '-') {
            currentSequence.push(Decr);
        } else if (!isNaN(src[i])) {
            if (i + 1 === src.length) {
                return `Syntax error: missing operator '!' or '?' after digit '${src[i]}' at position ${i}`
            }
            const digit = +src[i];
            const operator = src[++i];
            if (operator === '!') {
                currentSequence.push(Write(digit));
            } else if (operator === '?') {
              currentSequence.push(AssertRead(digit));
            } else if (operator === '~') {
              currentSequence.push(AssertReadNeq(digit));
            } else {
                return `Syntax error: unexpected '${operator}', expected '!' or '?' at position ${i}`
            }
        } else {
            return `Syntax error: unknown character '${src[i]}' at position ${i}`
        }
    }
    let expr = Sequence(currentSequence)
    while (stack !== null) {
        const sc = stack.symbol;
        if (sc === '(') {
            return `Syntax error: mismatched '(' at position ${stack.position}`
        } else if (sc === '[') {
            return `Syntax error: mismatched '[' at position ${stack.position}`
        } else if (sc === '|') {
            expr = Alt(stack.left, expr);
            stack = stack.prev;
        } else {
            return `The impossible happened at position ${stack.position}`
        }
    }
    return expr;
}

function parseTape(src) {
    src = src.replace(/\s*/g, '');
    let tape = Immutable.Map();
    if (0 < src.length && src[0] === ',') {
        // Parse comma-separated list
        let i = 0;
        let j = 1;
        while (j < src.length) {
            // Each iteration reads one number
            let j0 = j;
            const c = src[j];
            // There must be at least on digit.
            if (isNaN(c)) {
                return `Syntax error: unexpected '${c}' at position ${j}, expected digit`
            }
            j++;
            // Find the next comma and check that there are only digits in the middle.
            while (j < src.length) {
                const c = src[j];
                if (c === ',') {
                    break;
                } else if (isNaN(c)) {
                    return `Syntax error: unexpected '${c}' at position ${j}, expected digit or comma`
                } else {
                    j++;
                }
            }
            tape = tape.set(i++, +src.slice(j0,j));
            j++;
        }
    } else {
        // One digit for every cell
        for (let i = 0; i < src.length; i++) {
            let digit = +src[i];
            if (isNaN(digit)) {
                return `Syntax error: '${digit}' is not a digit, at position ${i}`
            }
            tape = tape.set(i, digit);
        }
    }
    return tape
}

function printTapeOf(state, commas) {
    let tape = Immutable.Range(0, 10).map(i => state.tape.get(i, 0)).toArray();
    let result;
    if (commas || tape.some(n => !(0 <= n && n <= 9))) {
        const i = tape.findLastIndex((n) => n !== 0);
        if (i === -1) {
            return ',0'
        } else {
            tape = tape.slice(0,i+1);
            return [[''], tape].join(',')
        }
    } else {
        return tape.join('')
    }
}

function run(element) {
    const machine = element.parentNode.parentNode;
    const exprSrc = machine.querySelector("[name='expr']").value;
    const tapeSrc = machine.querySelector("[name='tape']").value;

    const exprErrorElement = machine.querySelector('.expr-error');
    const tapeErrorElement = machine.querySelector('.tape-error');
    const outputErrorElement = machine.querySelector('.output-error');
    const outputElement = machine.querySelector("[name='output']");

    exprErrorElement.toggleAttribute('hidden', true);
    tapeErrorElement.toggleAttribute('hidden', true);
    outputErrorElement.toggleAttribute('hidden', true);
    outputElement.value = '';

    let error = false;

    const expr = parseExpr(exprSrc)
    if (typeof expr === 'string') {
        exprErrorElement.textContent = expr;
        exprErrorElement.toggleAttribute('hidden', false);
        error = true;
    }

    const tape = parseTape(tapeSrc);
    if (typeof tape === 'string') {
        tapeErrorElement.textContent = tape;
        tapeErrorElement.toggleAttribute('hidden', false);
        error = true;
    }

    if (error) { return }

    const tapeState = { tape: tape, head: 0 };
    let finalState
    let err
    try {
        finalState = expr(tapeState).first();
    } catch (state) {
        if (state.hasOwnProperty('tape')) {
            finalState = state;
            outputErrorElement.textContent = "🦖 Walked out of bounds [-100, 100]";
            outputErrorElement.toggleAttribute('hidden', false);
        } else {
            err = state
        }
    }
    if (!finalState) {
        if (err) {
            outputErrorElement.textContent = `Machine failed. (${err.name}: ${err.message})`;
            outputErrorElement.toggleAttribute('hidden', false);
            throw err
        } else {
            outputErrorElement.textContent = "Machine failed: there is no path satisfying all assertions"
            outputErrorElement.toggleAttribute('hidden', false);
        }
    } else {
        commas = (0 < tapeSrc.length && tapeSrc[0] === ',');
        outputElement.value = printTapeOf(finalState, commas);
    }
}

// Populate the machine examples
document.addEventListener("DOMContentLoaded", () => {
    if (!('content' in document.createElement('template'))) {
        // templates not supported by browser
        return
    }
    const template = document.querySelector('#template');
    for (const machine of document.querySelectorAll('.machine-src')) {
        const expr = machine.querySelector('.expr-src').textContent;
        const tape = machine.querySelector('.tape-src').textContent;

        const newMachine = template.content.cloneNode(true);
        newMachine.querySelector('[name="expr"]').textContent = expr;
        newMachine.querySelector('[name="tape"]').setAttribute("value", tape);
        machine.replaceWith(newMachine);
    }
})
